'''
Exercice 2bis 
Format de sortie
=> script pour que le fichier de sortie soit sous le format json
'''

import json 

#Contenu que l'on veut mettre dans le fichier 

contenu = {}

# Ouvrir un fichier en écriture

with open("contenu.json", "w") as f: 
	#Écriture du contenu du dictionnaire "contenu" dans le fichier au format json 
	json.dump(personne,f) 
	

'''
Correction mise en ligne : 

import json

from dataclasses import asdict
# module, on importe asdict.

from datastructures import Corpus, Article
# on fait un import d'un autre programme

def write_json(corpus:Corpus, destination:str):

#Fonction qui permet de convertir un objet 'Corpus' en un fichier JSON et de le sauvegarder dans un emplacement donné.

    with open(destination, "w") as fout:
        json.dump(asdict(corpus), fout)
        # méthode "asdict" est appelé sur l'objet 'corpus' pour convertir cet objet en un dictionnaire Python. Cela permet ensuite décrire les données du corpus en JSON en utilisant la fonction 'json.dump()'.
'''
