import os
import glob 

def get_month_files():
    mois_input = input("Entrez le/les mois souhaités en toutes lettres et séparés par une virgule. Si aucune, pressez entrer: ").lower()
    fichiers_mois = []
    dos_base = "chemin_de_base/"  # remplacer par le chemin de base souhaité
    dico_mois = {"janvier": "01", "février": "02", "mars": "03", "avril": "04", "mai": "05", "juin": "06",
                 "juillet": "07", "août": "08", "septembre": "09", "octobre": "10", "novembre": "11", "décembre": "12"}
    
    if mois_input:
        mois_list = mois_input.split(',')
        for mois in mois_list:
            mois_dir = os.path.join(dos_base, dico_mois[mois.strip()])
            for root, dirs, files in os.walk(mois_dir):
                for file in files:
                    if file.endswith('.xml') and not file.startswith('fil'):
                        fichiers_mois.append(os.path.join(root, file))
    else:
        mois_dir = dos_base
       
    print(fichiers_mois)  
    return fichiers_mois
    
# Catégorie
def get_category_files():
    cat_input = input("Entrez la/les catégories voulues séparés par une virgule. Entrez 'liste' pour avoir la liste des catégories ou laissez vide: ")
    fichiers_cat = []
    dos_base = "chemin_de_base/" # remplacer par le chemin de base souhaité
    dico_cat = {"une" : "0,2-3208,1-0,0", "international":"0,2-3210,1-0,0", "europe" : "0,2-3214,1-0,0", "societe" : "0,2-3224,1-0,0", "idees":"0,2-3232,1-0,0", "economie" : "0,2-3234,1-0,0",  "actualite-medias":"0,2-3236,1-0,0", "sport": "0,2-3241,1-0,0","planetet" : "0,2-3244,1-0,0","culture" : "0,2-3246,1-0,0","livres" : "0,2-3260,1-0,0","cinema" : "0,2-3476,1-0,0", "voyage" : "0,2-3546,1-0,0","technologies":"0,2-651865,1-0,0","politique":"0,57-0,64-823353,0", "sciences":"env_sciences"}

    if cat_input == "liste":
        for cat in dico_cat:
            print(cat.split(":")[0])
    else:
        if cat_input:
            cat_list = cat_input.split(',')
            for categorie in cat_list:
                categorie = categorie.strip()
                if categorie not in dico_cat:
                    print(f"Catégorie(s) invalide(s).")
                else:
                    category_dir = os.path.join(dos_base, dico_cat[categorie], "*.xml")
                    for file in glob.glob(category_dir):
                        if not os.path.basename(file).startswith('fil'):
                            fichiers_cat.append(file)
        else:
            category_dir = os.path.join(dos_base, "*.xml")
            for file in glob.glob(category_dir):
                if not os.path.basename(file).startswith('fil'):
                    fichiers_cat.append(file)

    print (fichiers_cat)
    return fichiers_cat
    
def main():
	get_month_files()
	get_category_files()
	
