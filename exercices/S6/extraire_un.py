import sys
import os
import feedparser

def extract_articles(xml_file):
    # Vérifie si le fichier XML existe bien
    
    if not os.path.isfile(xml_file):
        print(f"{xml_file} n'existe pas.")
        return


    # Parse XML file and extract article titles and descriptions
    feed = feedparser.parse(file_path)
    for article in feed.entries:
        title = article.title
        description = article.description
        print(f"Title: {title}\nDescription: {description}\n")
 
 
if __name__ == "__main__":
    # Check if file argument was provided
    if len(sys.argv) < 2:
        print("Please provide the name of an XML file.")
    else:
        # Extract articles from file
        file_path = sys.argv[1]
        extract_articles(file_path)

'''
Pour importer ce programme dans un autre programme python : 

from my_program import extract_articles
extract_articles('sample.xml')
 

'''
