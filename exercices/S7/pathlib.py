import os
from pathlib import Path

# Définir la liste des catégories
dico_cat={"une":"0,2-3208,1-0,0", 
          "international":"0,2-3210,1-0,0", 
          "europe":"0,2-3214,1-0,0",
          "societe":"0,2-3224,1-0,0", 
          "idees":"0,2-3232,1-0,0",
          "economie":"0,2-3234,1-0,0",
          "actualite-medias":"0,2-3236,1-0,0",
          "sport":"0,2-3242,1-0,0",
          "planete":"0,2-3244,1-0,0",
          "culture":"0,2-3246,1-0,0",
          "livres":"0,2-3260,1-0,0",
          "cinema":"0,2-3476,1-0,0",
          "voyage":"0,2-3546,1-0,0",
          "technologies":"0,2-651865,1-0,0",
          "politique":"0,57-0,64-823353,0",
          "sciences":"env_sciences"}

# Définir le chemin du dossier contenant les fichiers à traiter
folder_path = Path('/chemin/vers/le/dossier')

# Parcourir tous les fichiers du dossier et de ses sous-dossiers
for file_path in folder_path.glob('**/*'):
    # Vérifier si le fichier est un fichier régulier (et non un dossier)
    if file_path.is_file():
        # Vérifier si le fichier est dans la liste des catégories
        if file_path.suffix[1:] in dico_cat:
            # Effectuer le traitement sur le fichier ici
            print(f'Traitement du fichier {file_path}')
