import spacy

from collections import namedtuple
from dataclasses import dataclass

from datastructures import Token, Article
# les classes 'Token' et 'Article' ne sont pas définies dasn cet extrait de code, elles sont définies dans le module datastructures.py que l'on importe. 

def create_parser():
    return spacy.load("fr_core_news_md")
    # la méthode "load()" permet de charger le modèle et de retourner l'objet 'Language'.


def analyse_article(parser, article: Article) -> Article:
    result = parser( (article.titre or "" ) + "\n" + (article.description or ""))
    output = []
    for token in result:
        output.append(Token(token.text, token.lemma_, token.pos_))
    article.analyse = output
    return article

